package com.order_product.devcamporder.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.order_product.devcamporder.model.CCustomer;
import com.order_product.devcamporder.model.COrder;
import com.order_product.devcamporder.model.CProduct;
import com.order_product.devcamporder.repository.ICustomerRepository;
import com.order_product.devcamporder.repository.IOrderRepository;


@RequestMapping("/")
@RestController
@CrossOrigin(value = "*" , maxAge = -1)
public class AllController {
    @Autowired
    ICustomerRepository iCustomerRepository;
    @Autowired
    IOrderRepository iOrderRepository;
    
    @GetMapping("/devcamp-users")
    public ResponseEntity<List<CCustomer>> getUserList() {
        try {
            List<CCustomer> listUser = new ArrayList<CCustomer>();
            iCustomerRepository.findAll().forEach(listUser::add);
            return new ResponseEntity<List<CCustomer>>(listUser, HttpStatus.OK);
        } catch(Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/devcamp-orders")
    public ResponseEntity<Set<COrder>> getOrderListByCustomerId(@RequestParam(required = true) long userId) {
        try {
            CCustomer vCustomer = iCustomerRepository.findById(userId);
            if(vCustomer != null){
                return new ResponseEntity<>(vCustomer.getOrders(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch(Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/devcamp-products")
    public ResponseEntity<Set<CProduct>> getProductListByOrderId(@RequestParam(required = true) long orderId) {
        try {
            COrder vOrder = iOrderRepository.findById(orderId);
            if(vOrder != null){
                return new ResponseEntity<>(vOrder.getProducts(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch(Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
