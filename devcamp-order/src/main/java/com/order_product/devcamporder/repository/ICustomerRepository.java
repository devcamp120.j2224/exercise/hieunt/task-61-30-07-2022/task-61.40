package com.order_product.devcamporder.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.order_product.devcamporder.model.CCustomer;


public interface ICustomerRepository extends JpaRepository<CCustomer, Long>{
    CCustomer findById(long id);
}
