package com.order_product.devcamporder.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.order_product.devcamporder.model.CProduct;

public interface IProductRepository extends JpaRepository<CProduct, Long> {
    
}
